package com.satyadara.sigah.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.satyadara.sigah.R;
import com.satyadara.sigah.model.Cost;
import com.satyadara.sigah.model.DataCost;
import com.satyadara.sigah.model.Result;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

/**
 * Created by Satya Syahputra on 25/04/2018.
 */

@Data
public class CostAdapter extends Adapter<CostAdapter.CostViewHolder> {
    Context context;
    List<Cost> list;

    public CostAdapter(Context context, List<Cost> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public CostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemRow = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_cost, parent, false);
        return new CostViewHolder(itemRow);
    }

    @Override
    public void onBindViewHolder(CostViewHolder holder, int position) {
        holder.name.setText(list.get(position).getCostName());
        holder.price.setText(list.get(position).getCostPrice());
        holder.description.setText(list.get(position).getCostDescription());
        if(holder.name.getText().toString().equals("Executive"))
            holder.image.setImageResource(R.drawable.executive);
        else if(holder.name.getText().toString().equals("Executive Deluxe"))
            holder.image.setImageResource(R.drawable.executivedeluxe);
        else if(holder.name.getText().toString().equals("Junior Suite"))
            holder.image.setImageResource(R.drawable.juniorsuites);
        else if(holder.name.getText().toString().equals("Double Deluxe"))
            holder.image.setImageResource(R.drawable.doublesuite);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class CostViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView price;
        TextView description;
        ImageView image;

        public CostViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            price = (TextView) itemView.findViewById(R.id.price);
            description = (TextView) itemView.findViewById(R.id.description);
            image = (ImageView) itemView.findViewById(R.id.Imageviewcost);
        }
    }

}
