package com.satyadara.sigah.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.satyadara.sigah.R;
import com.satyadara.sigah.model.Cost;
import com.satyadara.sigah.model.HistoryReservation;

import java.util.List;

/**
 * Created by Andika on 5/1/2018.
 */

public class HistoryReservationAdapter extends RecyclerView.Adapter<HistoryReservationAdapter.CostViewHolder> {
    Context context;
    List<HistoryReservation> list;

    public HistoryReservationAdapter(Context context, List<HistoryReservation> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public HistoryReservationAdapter.CostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemRow = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_cost, parent, false);
        return new HistoryReservationAdapter.CostViewHolder(itemRow);
    }

    @Override
    public void onBindViewHolder(HistoryReservationAdapter.CostViewHolder holder, int position) {
        holder.checkIn.setText(list.get(position).getDetailresCheckIn());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class CostViewHolder extends RecyclerView.ViewHolder {
        TextView checkIn;


        public CostViewHolder(View itemView) {
            super(itemView);
            checkIn = (TextView) itemView.findViewById(R.id.checkindate);

        }
    }
}
