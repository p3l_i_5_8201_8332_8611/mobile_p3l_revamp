package com.satyadara.sigah;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.satyadara.sigah.api.ApiClient;
import com.satyadara.sigah.httprequest.Api;
import com.satyadara.sigah.model.UserList;

import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditAccountActivity extends AppCompatActivity {
    private EditText namatxt,telptxt,addrtxt,emailtxt;
    private Button ubahbtn;
    public String email;
    public String nama ;
    public String telp ;
    public String address;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_account);
        Intent intent=getIntent();
        email = intent.getStringExtra("email");
        nama = intent.getStringExtra("nama");
        telp = intent.getStringExtra("telp");
        address = intent.getStringExtra("addr");

        Toast.makeText(EditAccountActivity.this,email,Toast.LENGTH_SHORT).show();
        namatxt = findViewById(R.id.ubahnamaaddress);
        telptxt = findViewById(R.id.ubahnamatelp);
        addrtxt = findViewById(R.id.ubahtextaddress);
        ubahbtn = findViewById(R.id.ubahbtnEC);
        namatxt.setText(nama);
        telptxt.setText(telp);
        addrtxt.setText(address);
        ubahbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                update();
            }
        });

    }

    private void update(){
        final ProgressDialog mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.show();

        nama = namatxt.getText().toString();
        telp = telptxt.getText().toString();
        address = addrtxt.getText().toString();
        Toast.makeText(EditAccountActivity.this,nama,Toast.LENGTH_SHORT).show();
        Toast.makeText(EditAccountActivity.this,telp,Toast.LENGTH_SHORT).show();
        Toast.makeText(EditAccountActivity.this,address,Toast.LENGTH_SHORT).show();
        Date user_created = Calendar.getInstance().getTime();
        Api api = ApiClient.getClient().create(Api.class);
        Call<UserList> call = api.ubah(address,email,nama,telp,user_created);
        call.enqueue(new Callback<UserList>() {
            @Override
            public void onResponse(Call<UserList> call, Response<UserList> response) {
                Toast.makeText(EditAccountActivity.this,"Success....",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(EditAccountActivity.this,MainActivity.class);
                intent.putExtra("email",email);
                intent.putExtra("address",address);
                intent.putExtra("nama",nama);
                intent.putExtra("telp",telp);
                startActivity(intent);
            }

            @Override
            public void onFailure(Call<UserList> call, Throwable t) {
                Toast.makeText(EditAccountActivity.this,"Failure....",Toast.LENGTH_SHORT).show();
            }
        });
    }

}