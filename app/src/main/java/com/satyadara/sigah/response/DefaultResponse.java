
package com.satyadara.sigah.response;

import com.satyadara.sigah.model.Cost;
import com.satyadara.sigah.model.Result;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DefaultResponse {

    private Integer value;
    private List<Cost> result = null;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public List<Cost> getResult() {
        return result;
    }

    public void setResult(List<Cost> result) {
        this.result = result;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
