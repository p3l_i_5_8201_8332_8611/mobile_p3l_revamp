package com.satyadara.sigah.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.satyadara.sigah.R;
import com.satyadara.sigah.adapter.CostAdapter;
import com.satyadara.sigah.adapter.HistoryReservationAdapter;
import com.satyadara.sigah.api.ApiClient;
import com.satyadara.sigah.httprequest.Api;
import com.satyadara.sigah.model.Cost;
import com.satyadara.sigah.model.CostView;
import com.satyadara.sigah.model.HistoryReservationList;
import com.satyadara.sigah.response.DefaultResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Andika on 5/1/2018.
 */

public class HistoryReservationFragment extends Fragment{
    private RecyclerView recyclerView;
    private List<Cost> listcost;
    private List<CostView> listroom;
    private String id;
    public HistoryReservationFragment() {
    }

    @SuppressLint("ValidFragment")
    public HistoryReservationFragment(String id) {
        this.id = id;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_history_reservation, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycleCost);
        Log.d("idsaha", id);
        Api api = ApiClient.getClient().create(Api.class);
        Call<HistoryReservationList> call = api.ShowCheckIn(id);
        call.enqueue(new Callback<HistoryReservationList>() {
            @Override
            public void onResponse(Call<HistoryReservationList> call, Response<HistoryReservationList> response) {
//                HistoryReservationAdapter myAdapter = new HistoryReservationAdapter(getContext(), response.body().getResult());
//                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//                recyclerView.setAdapter(myAdapter);
                Log.d("idsaha", response.body().toString());
            }

            @Override
            public void onFailure(Call<HistoryReservationList> call, Throwable t) {
                Log.d("idsaha", id);
            }
        });
        return view;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

}
