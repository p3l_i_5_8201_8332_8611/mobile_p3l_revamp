package com.satyadara.sigah.fragment;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.satyadara.sigah.R;
import com.satyadara.sigah.adapter.CostAdapter;
import com.satyadara.sigah.api.ApiClient;
import com.satyadara.sigah.httprequest.Api;
import com.satyadara.sigah.model.Cost;
import com.satyadara.sigah.model.CostView;
import com.satyadara.sigah.model.DataCost;
import com.satyadara.sigah.response.DefaultResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class CostFragment extends Fragment {
    private RecyclerView recyclerView;
    private List<Cost> listcost;
    private List<CostView> listroom;

    public CostFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cost, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycleCost);

        Api api = ApiClient.getClient().create(Api.class);
        Call<DefaultResponse> call = api.view();
        call.enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                Log.d("TESAJA", response.body().getResult().toString());
                CostAdapter myCostAdapter = new CostAdapter(getContext(), response.body().getResult());
                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                recyclerView.setAdapter(myCostAdapter);
            }
            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {
                Log.d("TESAJA", "TESESESESESESE");
            }
        });
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
}


