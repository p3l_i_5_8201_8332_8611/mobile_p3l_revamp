package com.satyadara.sigah.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.satyadara.sigah.EditAccountActivity;
import com.satyadara.sigah.R;
import com.satyadara.sigah.ReportTop5PromoActivity;
import com.satyadara.sigah.ReportTopFiveGuestActivity;
import com.satyadara.sigah.adapter.CostAdapter;
import com.satyadara.sigah.api.ApiClient;
import com.satyadara.sigah.httprequest.Api;
import com.satyadara.sigah.response.DefaultResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Andika on 5/2/2018.
 */

public class ReportFragment extends Fragment {
    private Button btnreporttop5reservation,btnreporttop5promo;
    public ReportFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_report, container, false);
        btnreporttop5promo = view.findViewById(R.id.tamu5total);
        btnreporttop5reservation = view.findViewById(R.id.tamu5reservasi);
        btnreporttop5promo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), ReportTop5PromoActivity.class);
                startActivity(i);
            }
        });
        btnreporttop5reservation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), ReportTopFiveGuestActivity.class);
                startActivity(i);
            }
        });
        return view;
    }
}
