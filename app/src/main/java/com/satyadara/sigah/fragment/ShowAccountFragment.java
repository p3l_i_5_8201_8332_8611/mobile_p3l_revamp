package com.satyadara.sigah.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.satyadara.sigah.EditAccountActivity;
import com.satyadara.sigah.R;

/**
 * Created by Andika on 5/1/2018.
 */

public class ShowAccountFragment extends Fragment {
    private String email,nama,telp,address;
    TextView textView1,textView2,textView3;
    public ShowAccountFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public ShowAccountFragment(String email, String nama, String telp, String address) {
        this.email = email;
        this.nama = nama;
        this.telp = telp;
        //Toast.makeText(AccountFragment.this, this.telp, Toast.LENGTH_LONG).show();
        this.address = address;
        //Log.d("user",nama);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_account,container,false);
        textView1 =  (TextView)rootview.findViewById(R.id.emailtxtview);
        textView2 =  (TextView)rootview.findViewById(R.id.userfullnametxtview);
        textView3 =  (TextView)rootview.findViewById(R.id.telptxtview);

        textView1.setText(email);
        textView2.setText(nama);
        textView3.setText(telp);
        Button btnUbah = rootview.findViewById(R.id.ubahbtn);
        btnUbah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), EditAccountActivity.class);
                i.putExtra("email",email);
                i.putExtra("nama",nama);
                i.putExtra("telp",telp);
                i.putExtra("addr",address);
                startActivity(i);
            }
        });
        return rootview;
    }
}