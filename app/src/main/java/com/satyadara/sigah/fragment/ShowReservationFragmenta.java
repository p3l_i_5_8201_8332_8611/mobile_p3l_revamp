package com.satyadara.sigah.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.satyadara.sigah.BatalPemesananActivity;
import com.satyadara.sigah.EditAccountActivity;
import com.satyadara.sigah.LoginActivity;
import com.satyadara.sigah.MainActivity;
import com.satyadara.sigah.R;
import com.satyadara.sigah.RegisterActivity;
import com.satyadara.sigah.api.ApiClient;
import com.satyadara.sigah.httprequest.Api;
import com.satyadara.sigah.model.BatalReservasiList;
import com.satyadara.sigah.model.ReservationdetailList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Andika on 5/1/2018.
 */

public class ShowReservationFragmenta extends Fragment {
    private String email,id,nama,telp,address;
    public TextView textView1,textView2,textView3,textView4,textView5,textView6,textView7,textView8,textView9,textView10,textView11,textView12;
    private Button btnBatal;
    public ShowReservationFragmenta() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public ShowReservationFragmenta(String email,String id) {
        this.email = email;
        this.id = id;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_show_reservation,container,false);
        textView1 =  (TextView)rootview.findViewById(R.id.textidreservasi);
        textView2 =  (TextView)rootview.findViewById(R.id.tglcetak);
        textView3 =  (TextView)rootview.findViewById(R.id.textnamareservasi);
        textView4 =  (TextView)rootview.findViewById(R.id.textalamatreservasi);
        textView5 =  (TextView)rootview.findViewById(R.id.checkinreservasi);
        textView6 =  (TextView)rootview.findViewById(R.id.checkoutreservasi);
        textView7 =  (TextView)rootview.findViewById(R.id.dewasareservasi);
        textView8 =  (TextView)rootview.findViewById(R.id.anakreservasi);
        textView9 =  (TextView)rootview.findViewById(R.id.tglbayarreservasi);
        textView10 =  (TextView)rootview.findViewById(R.id.jeniskamarreservasi);
        textView11 =  (TextView)rootview.findViewById(R.id.hargakamarreservasi);
//        textView12 =  (TextView)rootview.findViewById(R.id.textnamareservasi);
        Api api = ApiClient.getClient().create(Api.class);
        Call<ReservationdetailList> call = api.ShowNota(email);
        call.enqueue(new Callback<ReservationdetailList>() {
            @Override
            public void onResponse(Call<ReservationdetailList> call, Response<ReservationdetailList> response) {
                textView1.setText(response.body().getResult().get(0).getReservationId());
                textView2.setText(response.body().getResult().get(0).getUserFullname());
                textView4.setText(response.body().getResult().get(0).getUserAddress());
                textView3.setText(response.body().getResult().get(0).getDetailresCreated());
                textView5.setText(response.body().getResult().get(0).getDetailresCheckIn());
                textView6.setText(response.body().getResult().get(0).getDetailresCheckOut());
                textView7.setText(response.body().getResult().get(0).getReservationAdult());
                textView8.setText(response.body().getResult().get(0).getReservationChild());
                textView9.setText(response.body().getResult().get(0).getReservationCreated());
                textView10.setText(response.body().getResult().get(0).getCostName());
                textView11.setText(response.body().getResult().get(0).getCostPrice());
            }
            @Override
            public void onFailure(Call<ReservationdetailList> call, Throwable t) {

            }
        });
        btnBatal = rootview.findViewById(R.id.batalbtn);
        btnBatal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 Intent intent = new Intent(getActivity(),BatalPemesananActivity.class);
                 intent.putExtra("idd",id);
                 startActivity(intent);
            }
        });
      return rootview;
    }
}
