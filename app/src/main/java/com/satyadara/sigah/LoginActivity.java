package com.satyadara.sigah;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.satyadara.sigah.api.ApiClient;
import com.satyadara.sigah.httprequest.Api;
import com.satyadara.sigah.model.LoginList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private EditText emailTxt,passwordTxt;
    private Button btnLogin,btnRegister;
    public Api api;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        emailTxt = findViewById(R.id.edittextemail);
        passwordTxt = findViewById(R.id.edittextpassword);
        btnRegister = (Button) findViewById(R.id.createaccountbtn);
        btnLogin = (Button) findViewById(R.id.loginbtn);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Login();
            }
        });
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(i);
            }
        });
    }

    private void Login() {
        final ProgressDialog mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.show();

        String email = emailTxt.getText().toString();
        String password = passwordTxt.getText().toString();
//        Toast.makeText(LoginActivity.this,email,Toast.LENGTH_SHORT).show();
//        Toast.makeText(LoginActivity.this,password,Toast.LENGTH_SHORT).show();

        api = ApiClient.getClient().create(Api.class);
        Call<LoginList> call = api.loginRequest(email, password);
        call.enqueue(new Callback<LoginList>() {
            @Override
            public void onResponse(Call<LoginList> call, Response<LoginList> response) {
                if (response.isSuccessful()) {
                    LoginList result = response.body();
                    Log.d("LoginActivity", "response = " + new Gson().toJson(result));
                    if (result != null) {
                        Toast.makeText(LoginActivity.this, "Masuk", Toast.LENGTH_LONG).show();
                        mProgressDialog.dismiss();
                        Intent i = new Intent(LoginActivity.this,MainActivity.class);
                        //Intent i = new Intent(LoginActivity.this,MoveActivity.class);
                        i.putExtra("email",response.body().getResult().get(0).getUserEmail());
                        i.putExtra("nama",response.body().getResult().get(0).getUserFullname());
                        i.putExtra("telp",response.body().getResult().get(0).getUserTelp());
                        i.putExtra("address",response.body().getResult().get(0).getUserAddress());
                        i.putExtra("id",response.body().getResult().get(0).getUserId());
                        startActivity(i);
                    }
                }
                else{
                    Log.d("LoginActivity", "response = " + response.toString());
                    Toast.makeText(LoginActivity.this, "Password Salah", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<LoginList> call, Throwable t) {
                mProgressDialog.dismiss();
                Log.d("LoginActivity", "response = " + t.toString());
                Toast.makeText(LoginActivity.this, "Network Error", Toast.LENGTH_LONG).show();
            }
        });

    }
}