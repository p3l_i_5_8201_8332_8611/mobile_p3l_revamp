package com.satyadara.sigah.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Andika on 4/29/2018.
 */

public class ApiClient {
    //OkHttpClient httpClient = new OkHttpClient.Builder().build();

    public static final String BASE_URL = "http://192.168.0.7:81/crud/";
    private static Retrofit retrofit=null;

    public static Retrofit getClient() {
        if (retrofit == null) {
            Gson gson = new GsonBuilder().setLenient().create();
            retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).build();
        }
        return retrofit;
    }
}

