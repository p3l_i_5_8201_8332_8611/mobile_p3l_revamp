package com.satyadara.sigah;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.satyadara.sigah.api.ApiClient;
import com.satyadara.sigah.httprequest.Api;
import com.satyadara.sigah.model.BatalReservasiList;
import com.satyadara.sigah.model.ReservationdetailList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BatalPemesananActivity extends AppCompatActivity {
    Button btnya,btntdk;
    String id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_batal_pemesanan);
        btnya = findViewById(R.id.btnya);
        btntdk = findViewById(R.id.btntdk);
        Intent intent=getIntent();
        id = intent.getStringExtra("idd");
        btnya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Api api = ApiClient.getClient().create(Api.class);
                Call<BatalReservasiList> call = api.BatalReservasi(id);
                call.enqueue(new Callback<BatalReservasiList>() {
                    @Override
                    public void onResponse(Call<BatalReservasiList> call, Response<BatalReservasiList> response) {
                        Intent intent = new Intent(BatalPemesananActivity.this,MainActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<BatalReservasiList> call, Throwable t) {
                        Toast.makeText(BatalPemesananActivity.this, "Error Jaringan", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
        btntdk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BatalPemesananActivity.this,MainActivity.class);
                intent.putExtra("id",id);
                startActivity(intent);
            }
        });
    }
}
