package com.satyadara.sigah;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.satyadara.sigah.api.ApiClient;
import com.satyadara.sigah.fragment.CostFragment;
import com.satyadara.sigah.fragment.HistoryReservationFragment;
import com.satyadara.sigah.fragment.ReportFragment;
import com.satyadara.sigah.fragment.ShowAccountFragment;
import com.satyadara.sigah.fragment.ShowReservationFragmenta;
import com.satyadara.sigah.httprequest.Api;
import com.satyadara.sigah.model.DataCost;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private Fragment mFragment;
    private FragmentManager mFragmentManager;
    private String email,nama,telp,address,iduser;
    public FragmentTransaction mFragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent=getIntent();
        email = intent.getStringExtra("email");
        nama = intent.getStringExtra("nama");
        telp = intent.getStringExtra("telp");
        address = intent.getStringExtra("address");
        iduser = intent.getStringExtra("id");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mFragmentManager = getSupportFragmentManager();
//        CostFragment mCostFragment = new CostFragment();
//        mFragmentTransaction.replace(R.id.fragment, mCostFragment, CostFragment.class.getSimpleName());
//        mFragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        mFragmentTransaction = mFragmentManager.beginTransaction();

        if (id == R.id.nav_camera) {
            // Handle the camera action
            CostFragment mCostFragment = new CostFragment();
            mFragmentTransaction.replace(R.id.fragment, mCostFragment, CostFragment.class.getSimpleName());
        } else if (id == R.id.nav_gallery) {
            HistoryReservationFragment mHistoryFragment = new HistoryReservationFragment(iduser);
            mFragmentTransaction.replace(R.id.fragment, mHistoryFragment, HistoryReservationFragment.class.getSimpleName());
        } else if (id == R.id.nav_slideshow) {
            ShowReservationFragmenta mShowResFragment = new ShowReservationFragmenta(email,iduser);
            mFragmentTransaction.replace(R.id.fragment, mShowResFragment, ShowReservationFragmenta.class.getSimpleName());
        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {
            ReportFragment mReportfragment = new ReportFragment();
            mFragmentTransaction.replace(R.id.fragment,mReportfragment,ReportFragment.class.getSimpleName());
        } else if (id == R.id.nav_send) {
            ShowAccountFragment mShowAccountFragment = new ShowAccountFragment(email, nama, telp, address);
            mFragmentTransaction.replace(R.id.fragment,mShowAccountFragment,ShowAccountFragment.class.getSimpleName());
        }

        mFragmentTransaction.commit();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

}
