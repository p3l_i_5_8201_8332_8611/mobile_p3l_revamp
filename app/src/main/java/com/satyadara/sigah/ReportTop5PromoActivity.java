package com.satyadara.sigah;

import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.satyadara.sigah.api.ApiClient;
import com.satyadara.sigah.httprequest.Api;
import com.satyadara.sigah.model.ReportTop5;
import com.satyadara.sigah.model.ReportTop5List;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportTop5PromoActivity extends AppCompatActivity {

    private TextView txt11,txt12,txt13,txt14,txttahun;
    //public TextView no = new TextView(this);
    private String tahun="2018";
    public int i=0;
    public ArrayList<ReportTop5> list;
    private TableLayout tbllayout;
    public TableRow row;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_top5_promo);
        tbllayout =(TableLayout) findViewById(R.id.tblview);
        TableRow tr_head = new TableRow(this);
        tr_head.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT,
                        TableRow.LayoutParams.FILL_PARENT));
        TextView nomor = new TextView(this);
        nomor.setText("No");
        tr_head.addView(nomor);
        TextView nama = new TextView(this);
        nama.setText("Nama");
        tr_head.addView(nama);
        TextView jumlah = new TextView(this);
        jumlah.setText("Jumlah Transaksi");
        tr_head.addView(jumlah);
        TextView total = new TextView(this);
        total.setText("Total");
        tr_head.addView(total);
        tbllayout.addView(tr_head);
        txttahun = findViewById(R.id.txtTahun);
        txttahun.setText(tahun);
        Api api = ApiClient.getClient().create(Api.class);
        Call<ReportTop5List> call = api.showReporttop5promo(tahun);
        call.enqueue(new Callback<ReportTop5List>() {
            @Override
            public void onResponse(Call<ReportTop5List> call, Response<ReportTop5List> response) {
                for (i=0;i<response.body().getResult().size();) {
                    txt11 = new TextView(ReportTop5PromoActivity.this);
                    txt12 = new TextView(ReportTop5PromoActivity.this);
                    txt13 =  new TextView(ReportTop5PromoActivity.this);
                    txt14 = new TextView(ReportTop5PromoActivity.this);
                    //row = (TableRow) findViewById(R.id.tblrow);
                    row = new TableRow(ReportTop5PromoActivity.this);
                    String nama = response.body().getResult().get(i).getUserFullname();
                    String jumlah = response.body().getResult().get(i).getJumlahreservasi();
                    Log.d("Tesahsa",response.body().getResult().get(i).getJumlahreservasi());
                    String Total = response.body().getResult().get(i).getTotalreservasi();
                    i++;
                    if(txt14.getParent() != null)
                        ((ViewGroup)txt14.getParent()).removeView(txt14);
                    txt14.setText(String.valueOf(i));
                    row.addView(txt14);
                    if(txt11.getParent() != null)
                        ((ViewGroup)txt11.getParent()).removeView(txt11);
                    txt11.setText(nama);
                    row.addView(txt11);
                    if(txt12.getParent() != null)
                        ((ViewGroup)txt12.getParent()).removeView(txt12);
                    txt12.setText(jumlah);
                    row.addView(txt12);
                    if(txt13.getParent() != null)
                        ((ViewGroup)txt13.getParent()).removeView(txt13);
                    txt13.setText(Total);
                    row.addView(txt13);
                    if(row.getParent()!=null)
                        ((ViewGroup)row.getParent()).removeView(row);
                    tbllayout.addView(row);
                }
            }

            @Override
            public void onFailure(Call<ReportTop5List> call, Throwable t) {

            }

        });
    }
}
