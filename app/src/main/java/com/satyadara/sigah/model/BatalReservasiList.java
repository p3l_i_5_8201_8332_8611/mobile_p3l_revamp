package com.satyadara.sigah.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Andika on 5/1/2018.
 */

public class BatalReservasiList {
    @SerializedName("value")
    @Expose
    private Integer value;
    @SerializedName("result")
    @Expose
    private List<BatalReservasi> result = null;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public List<BatalReservasi> getResult() {
        return result;
    }

    public void setResult(List<BatalReservasi> result) {
        this.result = result;
    }
}
