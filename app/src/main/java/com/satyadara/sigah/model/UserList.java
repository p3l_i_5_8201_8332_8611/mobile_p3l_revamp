package com.satyadara.sigah.model;

import java.util.List;

/**
 * Created by Andika on 5/1/2018.
 */

public class UserList {
    public List<User> getUserData() {
        return userData;
    }

    public void setUserData(List<User> userData) {
        this.userData = userData;
    }

    private List<User> userData;
}