package com.satyadara.sigah.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Andika on 5/2/2018.
 */

public class ReportTop5 {
    @SerializedName("user_fullname")
    @Expose
    private String userFullname;
    @SerializedName("jumlahreservasi")
    @Expose
    private String jumlahreservasi;
    @SerializedName("totalreservasi")
    @Expose
    private String totalreservasi;

    public String getUserFullname() {
        return userFullname;
    }

    public void setUserFullname(String userFullname) {
        this.userFullname = userFullname;
    }

    public String getJumlahreservasi() {
        return jumlahreservasi;
    }

    public void setJumlahreservasi(String jumlahreservasi) {
        this.jumlahreservasi = jumlahreservasi;
    }

    public String getTotalreservasi() {
        return totalreservasi;
    }

    public void setTotalreservasi(String totalreservasi) {
        this.totalreservasi = totalreservasi;
    }

}

