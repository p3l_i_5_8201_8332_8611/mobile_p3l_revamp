package com.satyadara.sigah.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Andika on 5/1/2018.
 */

public class Login {
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("user_address")
    @Expose
    private String userAddress;
    @SerializedName("user_cc_name")
    @Expose
    private String userCcName;
    @SerializedName("user_cc_number")
    @Expose
    private String userCcNumber;
    @SerializedName("user_created")
    @Expose
    private String userCreated;
    @SerializedName("user_email")
    @Expose
    private String userEmail;
    @SerializedName("user_fullname")
    @Expose
    private String userFullname;
    @SerializedName("user_ktp")
    @Expose
    private String userKtp;
    @SerializedName("user_password")
    @Expose
    private String userPassword;
    @SerializedName("user_telp")
    @Expose
    private String userTelp;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("branch_id")
    @Expose
    private String branchId;
    @SerializedName("role_id")
    @Expose
    private String roleId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getUserCcName() {
        return userCcName;
    }

    public void setUserCcName(String userCcName) {
        this.userCcName = userCcName;
    }

    public String getUserCcNumber() {
        return userCcNumber;
    }

    public void setUserCcNumber(String userCcNumber) {
        this.userCcNumber = userCcNumber;
    }

    public String getUserCreated() {
        return userCreated;
    }

    public void setUserCreated(String userCreated) {
        this.userCreated = userCreated;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserFullname() {
        return userFullname;
    }

    public void setUserFullname(String userFullname) {
        this.userFullname = userFullname;
    }

    public String getUserKtp() {
        return userKtp;
    }

    public void setUserKtp(String userKtp) {
        this.userKtp = userKtp;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserTelp() {
        return userTelp;
    }

    public void setUserTelp(String userTelp) {
        this.userTelp = userTelp;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

}
