package com.satyadara.sigah.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Andika on 5/1/2018.
 */

public class HistoryReservation {
    @SerializedName("detailres_check_in")
    @Expose
    private String detailresCheckIn;

    public String getDetailresCheckIn() {
        return detailresCheckIn;
    }

    public void setDetailresCheckIn(String detailresCheckIn) {
        this.detailresCheckIn = detailresCheckIn;
    }

}