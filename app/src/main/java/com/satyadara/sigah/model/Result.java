package com.satyadara.sigah.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Andika on 4/29/2018.
 */

public class Result {
    private String costId;
    private String costCapacity;
    private String costCreated;
    private String costDescription;
    private Object costImage;
    private String costName;
    private String costPrice;
    private String costStatus;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getCostId() {
        return costId;
    }

    public void setCostId(String costId) {
        this.costId = costId;
    }

    public String getCostCapacity() {
        return costCapacity;
    }

    public void setCostCapacity(String costCapacity) {
        this.costCapacity = costCapacity;
    }

    public String getCostCreated() {
        return costCreated;
    }

    public void setCostCreated(String costCreated) {
        this.costCreated = costCreated;
    }

    public String getCostDescription() {
        return costDescription;
    }

    public void setCostDescription(String costDescription) {
        this.costDescription = costDescription;
    }

    public Object getCostImage() {
        return costImage;
    }

    public void setCostImage(Object costImage) {
        this.costImage = costImage;
    }

    public String getCostName() {
        return costName;
    }

    public void setCostName(String costName) {
        this.costName = costName;
    }

    public String getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(String costPrice) {
        this.costPrice = costPrice;
    }

    public String getCostStatus() {
        return costStatus;
    }

    public void setCostStatus(String costStatus) {
        this.costStatus = costStatus;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
