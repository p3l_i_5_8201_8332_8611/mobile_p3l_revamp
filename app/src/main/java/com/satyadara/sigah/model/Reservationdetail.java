package com.satyadara.sigah.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Andika on 5/1/2018.
 */

public class Reservationdetail {
    @SerializedName("reservation_id")
    @Expose
    private String reservationId;
    @SerializedName("user_fullname")
    @Expose
    private String userFullname;
    @SerializedName("user_address")
    @Expose
    private String userAddress;
    @SerializedName("detailres_created")
    @Expose
    private String detailresCreated;
    @SerializedName("detailres_check_in")
    @Expose
    private String detailresCheckIn;
    @SerializedName("detailres_check_out")
    @Expose
    private String detailresCheckOut;
    @SerializedName("reservation_adult")
    @Expose
    private String reservationAdult;
    @SerializedName("reservation_child")
    @Expose
    private String reservationChild;
    @SerializedName("reservation_created")
    @Expose
    private String reservationCreated;
    @SerializedName("cost_name")
    @Expose
    private String costName;
    @SerializedName("cost_price")
    @Expose
    private String costPrice;

    public String getReservationId() {
        return reservationId;
    }

    public void setReservationId(String reservationId) {
        this.reservationId = reservationId;
    }

    public String getUserFullname() {
        return userFullname;
    }

    public void setUserFullname(String userFullname) {
        this.userFullname = userFullname;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getDetailresCreated() {
        return detailresCreated;
    }

    public void setDetailresCreated(String detailresCreated) {
        this.detailresCreated = detailresCreated;
    }

    public String getDetailresCheckIn() {
        return detailresCheckIn;
    }

    public void setDetailresCheckIn(String detailresCheckIn) {
        this.detailresCheckIn = detailresCheckIn;
    }

    public String getDetailresCheckOut() {
        return detailresCheckOut;
    }

    public void setDetailresCheckOut(String detailresCheckOut) {
        this.detailresCheckOut = detailresCheckOut;
    }

    public String getReservationAdult() {
        return reservationAdult;
    }

    public void setReservationAdult(String reservationAdult) {
        this.reservationAdult = reservationAdult;
    }

    public String getReservationChild() {
        return reservationChild;
    }

    public void setReservationChild(String reservationChild) {
        this.reservationChild = reservationChild;
    }

    public String getReservationCreated() {
        return reservationCreated;
    }

    public void setReservationCreated(String reservationCreated) {
        this.reservationCreated = reservationCreated;
    }

    public String getCostName() {
        return costName;
    }

    public void setCostName(String costName) {
        this.costName = costName;
    }

    public String getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(String costPrice) {
        this.costPrice = costPrice;
    }

}