package com.satyadara.sigah.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Andika on 5/1/2018.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private String user_id;
    private String user_address;
    private String user_cc_name;
    private String user_cc_number;
    private Date user_created;
    private String user_fullname;
    private String user_ktp;
    private String user_password;
    private String user_telp;
    private String user_type;
    private String branch_id;
    private String role_id;
    private String user_email;
}