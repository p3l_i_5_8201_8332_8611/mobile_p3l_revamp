package com.satyadara.sigah.model;

/**
 * Created by Andika on 4/29/2018.
 */

public class CostView {
    String namaRoom;
    String hargaRoom;
    String deskripsi;
    Integer gambarRoom;

    public String getNamaRoom() {
        return namaRoom;
    }

    public void setNamaRoom(String namaRoom) {
        this.namaRoom = namaRoom;
    }

    public String getHargaRoom() {
        return hargaRoom;
    }

    public void setHargaRoom(String hargaRoom) {
        this.hargaRoom = hargaRoom;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public Integer getGambarRoom() {
        return gambarRoom;
    }

    public void setGambarRoom(Integer gambarRoom) {
        this.gambarRoom = gambarRoom;
    }

    public CostView(String namaRoom, String hargaRoom, String deskripsi, Integer gambarRoom) {

        this.namaRoom = namaRoom;
        this.hargaRoom = hargaRoom;
        this.deskripsi = deskripsi;
        this.gambarRoom = gambarRoom;
    }
}
