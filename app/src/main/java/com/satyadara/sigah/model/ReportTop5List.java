package com.satyadara.sigah.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Andika on 5/2/2018.
 */

public class ReportTop5List {
    @SerializedName("value")
    @Expose
    private Integer value;
    @SerializedName("result")
    @Expose
    private List<ReportTop5> result = null;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public List<ReportTop5> getResult() {
        return result;
    }

    public void setResult(List<ReportTop5> result) {
        this.result = result;
    }

}