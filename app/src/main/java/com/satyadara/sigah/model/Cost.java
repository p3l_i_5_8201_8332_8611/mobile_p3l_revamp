
package com.satyadara.sigah.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Builder;
import lombok.Data;

//@Data
//@Builder
public class Cost {
    @SerializedName("cost_id")
    @Expose
    private String costId;
    @SerializedName("cost_capacity")
    @Expose
    private String costCapacity;
    @SerializedName("cost_created")
    @Expose
    private String costCreated;
    @SerializedName("cost_description")
    @Expose
    private String costDescription;
    @SerializedName("cost_image")
    @Expose
    private Object costImage;
    @SerializedName("cost_name")
    @Expose
    private String costName;
    @SerializedName("cost_price")
    @Expose
    private String costPrice;
    @SerializedName("cost_status")
    @Expose
    private String costStatus;

    public String getCostId() {
        return costId;
    }

    public void setCostId(String costId) {
        this.costId = costId;
    }

    public String getCostCapacity() {
        return costCapacity;
    }

    public void setCostCapacity(String costCapacity) {
        this.costCapacity = costCapacity;
    }

    public String getCostCreated() {
        return costCreated;
    }

    public void setCostCreated(String costCreated) {
        this.costCreated = costCreated;
    }

    public String getCostDescription() {
        return costDescription;
    }

    public void setCostDescription(String costDescription) {
        this.costDescription = costDescription;
    }

    public Object getCostImage() {
        return costImage;
    }

    public void setCostImage(Object costImage) {
        this.costImage = costImage;
    }

    public String getCostName() {
        return costName;
    }

    public void setCostName(String costName) {
        this.costName = costName;
    }

    public String getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(String costPrice) {
        this.costPrice = costPrice;
    }

    public String getCostStatus() {
        return costStatus;
    }

    public void setCostStatus(String costStatus) {
        this.costStatus = costStatus;
    }

}
//    private String id;
//    private String name;
//    private Double large;
//    private Double price;
//    private String description;
//    private Integer capacity;
//    private String status;
//    private String image;
//    private Integer created;
   // private List<Object> seasons = null;
    //private List<Object> bedTypes = null;
    //private List<Object> facilities = null;
    //private Map<String, Object> additionalProperties = new HashMap<String, Object>();

//}
