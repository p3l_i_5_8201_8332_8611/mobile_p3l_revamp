package com.satyadara.sigah;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.satyadara.sigah.api.ApiClient;
import com.satyadara.sigah.httprequest.Api;
import com.satyadara.sigah.model.User;
import com.satyadara.sigah.model.UserList;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {
    private ProgressDialog progress;
    private String user_id;
    private String user_address;
    private String user_cc_name;
    private String user_cc_number;
    private Date user_created;
    private String user_fullname;
    private String user_ktp;
    private String user_password;
    private String user_telp;
    private String user_type;
    private String branch_id;
    private int role_id;
    private String user_email;

    //    @BindView(R.id.radiobtn)
    //    RadioGroup radiobtn;
    @BindView(R.id.user_addresstxt)
    AppCompatEditText user_addresstxt;
    @BindView(R.id.user_cc_nametxt) AppCompatEditText user_cc_nametxt;
    @BindView(R.id.user_cc_numbertxt) AppCompatEditText user_cc_numbertxt;
    @BindView(R.id.user_emailtxt) AppCompatEditText user_emailtxt;
    @BindView(R.id.user_fullnametxt) AppCompatEditText user_fullnametxt;
    @BindView(R.id.user_ktptxt) AppCompatEditText user_ktptxt;
    @BindView(R.id.user_passwordtxt) AppCompatEditText user_passwordtxt;
    @BindView(R.id.user_telptxt) AppCompatEditText user_telptxt;

    @OnClick(R.id.signupbtn) void daftar(){
        //Untuk menampilkan progress dialog
        progress = new ProgressDialog(this);
        progress.setCancelable(false);
        progress.setMessage("Loading...");
        progress.show();

        user_address = user_addresstxt.getText().toString();
        user_cc_name = user_cc_nametxt.getText().toString();
        user_cc_number = user_cc_numbertxt.getText().toString();
        user_email = user_emailtxt.getText().toString();
        user_fullname = user_fullnametxt.getText().toString();
        user_ktp = user_ktptxt.getText().toString();
        user_password = user_passwordtxt.getText().toString();
        user_telp = user_telptxt.getText().toString();
        role_id=1;
        branch_id="1";
        user_type="Personal";
        user_id = UUID.randomUUID().toString();
        user_created = new Date(System.currentTimeMillis());

        Api api = ApiClient.getClient().create(Api.class);
        Call<UserList> call = api.daftar(user_id,user_address,user_cc_name,user_cc_number,user_email,
                user_fullname,user_ktp,user_password,user_telp,user_created,user_type,branch_id,role_id);

        //Antrian call
        call.enqueue(new Callback<UserList>() {
            @Override
            public void onResponse(Call<UserList> call, Response<UserList> response) {
//                Log.d("response", response.body().toString());
                Intent intent = new Intent(RegisterActivity.this,LoginActivity.class);
                startActivity(intent);
            }

            @Override
            public void onFailure(Call<UserList> call, Throwable t) {
                t.printStackTrace();
                progress.dismiss();
                Toast.makeText(RegisterActivity.this,"Jaringan Error",Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
    }
}

