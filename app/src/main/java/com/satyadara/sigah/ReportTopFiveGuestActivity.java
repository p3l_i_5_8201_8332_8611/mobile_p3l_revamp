package com.satyadara.sigah;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.satyadara.sigah.api.ApiClient;
import com.satyadara.sigah.fragment.ReportFragment;
import com.satyadara.sigah.httprequest.Api;
import com.satyadara.sigah.model.ReportTop5;
import com.satyadara.sigah.model.ReportTop5List;
import com.satyadara.sigah.model.ReservationdetailList;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportTopFiveGuestActivity extends AppCompatActivity {

    private TextView txt11,txt12,txt13,txttahun;
    private String tahun="2018";
    private TableLayout tbllayout;
    public int i=0;
    public TableRow row;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_top_five_guest);
        tbllayout =(TableLayout) findViewById(R.id.tbllayout);
        TableRow tr_head = new TableRow(this);
        tr_head.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT,
                TableRow.LayoutParams.FILL_PARENT));
        TextView nomor = new TextView(this);
        nomor.setText("No");
        tr_head.addView(nomor);
        TextView nama = new TextView(this);
        nama.setText("Nama");
        tr_head.addView(nama);
        TextView jumlah = new TextView(this);
        jumlah.setText("Jumlah Transaksi");
        tr_head.addView(jumlah);
        tbllayout.addView(tr_head);
        txttahun = findViewById(R.id.txtTahun);
        txttahun.setText(tahun);
        Api api = ApiClient.getClient().create(Api.class);
        Call<ReportTop5List> call = api.showReporttop5(tahun);
        call.enqueue(new Callback<ReportTop5List>() {
            @Override
            public void onResponse(Call<ReportTop5List> call, Response<ReportTop5List> response) {
                for (i=0;i<response.body().getResult().size();) {
                    txt11 = new TextView(ReportTopFiveGuestActivity.this);
                    txt12 = new TextView(ReportTopFiveGuestActivity.this);
                    txt13 =  new TextView(ReportTopFiveGuestActivity.this);
                    //row = (TableRow) findViewById(R.id.tblrow);
                    row = new TableRow(ReportTopFiveGuestActivity.this);
                    String nama = response.body().getResult().get(i).getUserFullname();
                    String jumlah = response.body().getResult().get(i).getJumlahreservasi();
                    Log.d("Tesahsa",response.body().getResult().get(i).getJumlahreservasi());
                    String Total = response.body().getResult().get(i).getTotalreservasi();
                    i++;
                    if(txt13.getParent() != null)
                        ((ViewGroup)txt13.getParent()).removeView(txt13);
                    txt13.setText(String.valueOf(i));
                    row.addView(txt13);
                    if(txt11.getParent() != null)
                        ((ViewGroup)txt11.getParent()).removeView(txt11);
                    txt11.setText(nama);
                    row.addView(txt11);
                    if(txt12.getParent() != null)
                        ((ViewGroup)txt12.getParent()).removeView(txt12);
                    txt12.setText(jumlah);
                    row.addView(txt12);
                    if(row.getParent()!=null)
                        ((ViewGroup)row.getParent()).removeView(row);
                    tbllayout.addView(row);
                }
           }

            @Override
            public void onFailure(Call<ReportTop5List> call, Throwable t) {
                Log.d("teseses","tesaja");
            }
        });
    }
}
