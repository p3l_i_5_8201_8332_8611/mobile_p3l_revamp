package com.satyadara.sigah.httprequest;

import com.satyadara.sigah.model.BatalReservasiList;
import com.satyadara.sigah.model.DataCost;
import com.satyadara.sigah.model.HistoryReservationList;
import com.satyadara.sigah.model.LoginList;
import com.satyadara.sigah.model.ReportTop5List;
import com.satyadara.sigah.model.ReservationdetailList;
import com.satyadara.sigah.model.UserList;
import com.satyadara.sigah.response.DefaultResponse;

import java.util.Date;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Andika on 4/29/2018.
 */

public interface Api {
    @FormUrlEncoded
    @POST("insert.php")
    Call<UserList> daftar(@Field("user_id") String user_id,
                          @Field("user_address") String user_address,
                          @Field("user_cc_name") String user_cc_name,
                          @Field("user_cc_number") String user_cc_number,
                          @Field("user_email") String user_email,
                          @Field("user_fullname") String user_fullname,
                          @Field("user_ktp") String user_ktp,
                          @Field("user_password") String user_password,
                          @Field("user_telp") String user_telp,
                          @Field("user_created") Date user_created,
                          @Field("user_type") String user_type,
                          @Field("branch_id") String branch_id,
                          @Field("role_id") int role_id
    );


    @GET("viewRoomtype.php")
    Call<DefaultResponse> view();

//    @FormUrlEncoded
//    @POST("search.php")
//    Call<value> search(@Field("search") String search);

    @FormUrlEncoded
    @POST("login.php")
    Call<LoginList> loginRequest(@Field("user_email") String user_email,
                                 @Field("user_password") String user_password
    );
//
    @FormUrlEncoded
    @POST("UbahAkun.php")
    Call<UserList> ubah(  @Field("user_address") String user_address,
                              @Field("user_email") String user_email,
                              @Field("user_fullname") String user_fullname,
                              @Field("user_telp") String user_telp,
                              @Field("user_created") Date user_created
    );

    @FormUrlEncoded
    @POST("ViewHistoryReservation")
    Call<HistoryReservationList> ShowCheckIn(@Field("user_id") String user_id
    );

    @FormUrlEncoded
    @POST("ShowNotareservasi.php")
    Call<ReservationdetailList> ShowNota(@Field("user_email") String user_email
    );

    @FormUrlEncoded
    @POST("BatalReservasi.php")
    Call<BatalReservasiList> BatalReservasi(@Field("user_id") String user_id
    );

    @FormUrlEncoded
    @POST("laporantop5.php")
    Call<ReportTop5List> showReporttop5(@Field("tahun") String tahun
    );

    @FormUrlEncoded
    @POST("laporantop5promo.php")
    Call<ReportTop5List> showReporttop5promo(@Field("tahun") String tahun
    );
}
